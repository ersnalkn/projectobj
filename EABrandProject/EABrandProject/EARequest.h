//
//  EARequest.h
//  Arabam
//
//  Created by Ersun Alkan on 08/04/16.
//  Copyright © 2016 Ersun Alkan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EARequestManager.h"
#import "EAResponse.h"
#import <JSONModel.h>



@interface EARequest : JSONModel

typedef enum {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH
} HttpMethod;


@property(nonatomic)HttpMethod method;

-(NSString*)requestUrl;
-(NSDictionary*)urlParameters;
-(NSString*)getEndPointUrl;

@property(nonatomic,strong)NSArray* getUrlExtraParam;


@end



