//
//  EAAdressesTableViewController.m
//  EABrandProject
//
//  Created by ersun alkan on 19.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EAAdressesTableViewController.h"
#import "EAAddressesRequest.h"
#import "EARequestManager.h"

@interface EAAdressesTableViewController ()

@end

@implementation EAAdressesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Adreslerim";
    [self mainRequest];
}

-(void)mainRequest
{
    EAAddressesRequest* request=[EAAddressesRequest new];

    [EARequestManager request:request callback:^(id data, BOOL success) {
        if (success) {
            [self displayData];
        }else if ([data isKindOfClass:[NSString class]])
        {
            [self showAlertActionWithMessage:data];
        }
    }];
}

-(void)displayData
{
    self.title=@"";
    [self.tableView reloadData];
}


#pragma mark tableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//    EABrandProductDto* item = [self.brandResponse.products objectAtIndex:indexPath.row];
//    [cell setModelWithModel:item];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)showAlertActionWithMessage:(NSString*)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Hata" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Tamam"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


@end
