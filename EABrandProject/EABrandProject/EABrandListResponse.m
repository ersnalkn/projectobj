//
//  EABrandListResponse.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandListResponse.h"

@implementation EABrandListResponse

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"brandId": @"id"
                                                                  }];
}

@end



