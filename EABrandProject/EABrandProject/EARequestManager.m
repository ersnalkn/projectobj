//
//  EARequestManager.m
//  Arabam
//
//  Created by Ersun Alkan on 03/04/16.
//  Copyright © 2016 Ersun Alkan. All rights reserved.
//

#import "EARequestManager.h"
#import <JSONModel.h>
#import "EABrandListResponse.h"
#import <AFNetworkReachabilityManager.h>

@implementation EARequestManager


+(NSString *)buildURL:(NSString *)part
{
    NSMutableString *buffer = [NSMutableString stringWithString:@"https://api-dev.fiyuu.com.tr/"];

    if (part) {
        [buffer appendString:part];
    }
    return [NSString stringWithString:buffer];
}

+(void)addHeader:(NSString *)key value:(NSString *)value manager:(AFHTTPSessionManager*) manager
{
    if (key && value) {
        [manager.requestSerializer setValue:value forHTTPHeaderField:key];
    }
}


+(void)request:(EARequest*)request callback:(void (^)(id, BOOL))callback
{
    
    NSString *enurl = [self buildURL:[request getEndPointUrl]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:30];
    
    NSString* token=@"XpcLEEIMqWraiHxCwAxopAVu2ISiueadWye7S+5V2DvKmUExyGBH715VP/7ff+JbqySmxu2S8rNdlo1OoTRFdEMDB+Cojw==";
    
    [self addHeader:@"Accept-Charset" value:@"utf-8" manager:manager];
    [self addHeader:@"Content-Type" value:@"application/json" manager:manager];
    [self addHeader:@"token" value:token manager:manager];
    
    void (^success)(NSURLSessionTask *task, id responseObject) = ^(NSURLSessionTask *task, id responseObject)
    {
        EAResponse* baseResponse=[[EAResponse alloc] initWithDictionary:responseObject error:nil];
        if ([baseResponse.code isEqualToString:@"Success"])
        {
            NSLog(@"yey");
            callback(baseResponse.data,YES);
        }else if (baseResponse.message)
        {
            NSLog(@"Error Message: %@",baseResponse.message);
            callback(baseResponse.message,NO);
        }
        
    };
    void (^fail)(NSURLSessionTask *task, NSError *error) = ^(NSURLSessionTask *task, NSError *error)
    {
        if (error) {
            NSLog(@"%@",error);
            if (callback){
                callback(nil, NO);
            }
        }
    };

    switch (request.method){
        case GET:
            [manager GET:enurl parameters:nil progress:nil success:success failure:fail];
            break;
        case POST:
        {
            [manager POST:enurl parameters:[request urlParameters] progress:nil success:success failure:fail];
            break;
        }
        default:
            break;
    }
}

@end
