//
//  EARequest.m
//  Arabam
//
//  Created by Ersun Alkan on 08/04/16.
//  Copyright © 2016 Ersun Alkan. All rights reserved.
//

#import "EARequest.h"




@implementation EARequest


-(NSString*)requestUrl
{
    
    return @"/default";
}

-(EAResponse*)responseClass
{
    return [EAResponse new];
}

-(HttpMethod)method
{
    return GET;
}

-(BOOL)loginRequired
{
    return NO;
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{

    return YES;
}

-(NSString*)getEndPointUrl
{
    NSString* url=[self requestUrl];
    
    return url;
}



-(NSDictionary*)urlParameters
{
    NSMutableDictionary* dic=[NSMutableDictionary dictionaryWithDictionary:[self toDictionary]];
    [dic removeObjectForKey:@"method"];
    
    return dic;
    
}

@end
