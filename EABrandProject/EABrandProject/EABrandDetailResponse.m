//
//  EABrandDetailResponse.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandDetailResponse.h"


@implementation EABrandProductDto

+ (Class)classForCollectionProperty:(NSString *)propertyName
{
    if ([propertyName isEqual: @"images"])
    {
        return [NSString class];
    }
    return nil;
}


@end

@implementation EABrandDetailResponse

+ (Class)classForCollectionProperty:(NSString *)propertyName
{
    if ([propertyName isEqual: @"products"])
    {
        return [EABrandProductDto class];
    }
    return nil;
}

@end
