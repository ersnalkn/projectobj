//
//  EABrandListCell.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandDetailCell.h"
#import <UIImageView+AFNetworking.h>
#import <UIImage+AFNetworking.h>

@implementation EABrandDetailCell


-(void)setModelWithModel:(EABrandProductDto*)model
{
    self.brandImage.image=nil;
    self.brandNameLabel.text=model.name;
    self.brandCategoryLabel.text=model.summary;
    [self.brandImage setImageWithURL:[NSURL URLWithString:model.images.firstObject]];
    
}

@end
