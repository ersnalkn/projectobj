//
//  EABrandDetailTableViewController.h
//  EABrandProject
//
//  Created by ersun alkan on 19.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EABrandDetailTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic)IBOutlet UITableView* tableView;

@property(nonatomic,strong) NSString* brandId;

@end
