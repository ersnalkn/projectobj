//
//  EARequestManager.h
//  Arabam
//
//  Created by Ersun Alkan on 03/04/16.
//  Copyright © 2016 Ersun Alkan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "EARequest.h"
#import "EAResponse.h"

@interface EARequestManager : NSObject

+(void)request:(id)request callback:(void (^)(id, BOOL))callback;

@end
