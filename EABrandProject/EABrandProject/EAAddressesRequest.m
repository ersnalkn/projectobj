//
//  EAAddressesRequest.m
//  EABrandProject
//
//  Created by ersun alkan on 19.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EAAddressesRequest.h"

@implementation EAAddressesRequest

-(NSString*)requestUrl
{
    return @"customer/address/scan-2.0";
}


-(HttpMethod)method
{
    return POST;
}



@end
