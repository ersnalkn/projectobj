//
//  EAResponse.h
//  Arabam
//
//  Created by Ersun Alkan on 03/04/16.
//  Copyright © 2016 Ersun Alkan. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>


@interface EAResponse : JSONModel

@property(nonatomic)NSString<Optional>* code;
@property(nonatomic,strong)NSDictionary<Optional>* data;
@property(nonatomic,strong)NSString<Optional>* message;

@end
