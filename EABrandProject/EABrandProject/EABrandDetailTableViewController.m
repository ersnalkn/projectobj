//
//  EABrandDetailTableViewController.m
//  EABrandProject
//
//  Created by ersun alkan on 19.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandDetailTableViewController.h"
#import "EABrandDetailCell.h"
#import "EARequestManager.h"
#import "EABrandDetailRequest.h"

@interface EABrandDetailTableViewController ()

@property(nonatomic)EABrandDetailResponse* brandResponse;

@end

@implementation EABrandDetailTableViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self mainRequest];
}

-(void)mainRequest
{
    EABrandDetailRequest* request=[EABrandDetailRequest new];
    request.brandId=self.brandId;
    
    [EARequestManager request:request callback:^(id data, BOOL success) {
        if (success) {
            self.brandResponse=[[EABrandDetailResponse alloc] initWithDictionary:data error:nil];
            [self displayData];
        }
    }];
}

-(void)displayData
{
    self.title=self.brandResponse.name;
    [self.tableView reloadData];
}


#pragma mark tableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EABrandDetailCell* cell = [tableView dequeueReusableCellWithIdentifier:@"EABrandDetailCell" forIndexPath:indexPath];
    EABrandProductDto* item = [self.brandResponse.products objectAtIndex:indexPath.row];
    [cell setModelWithModel:item];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.brandResponse.products count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


@end
