//
//  EABrandDetailRequest.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandDetailRequest.h"
#import "EABrandDetailResponse.h"

@implementation EABrandDetailRequest

-(NSString*)requestUrl
{
    return @"customer/product/getBrandDetails2";
}

-(Class)responseClass
{
    return [EABrandDetailResponse class];
}

-(HttpMethod)method
{
    return POST;
}





@end
