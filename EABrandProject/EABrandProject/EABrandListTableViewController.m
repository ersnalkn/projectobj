//
//  EABrandListTableViewController.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandListTableViewController.h"
#import "EABrandListCell.h"
#import "EARequestManager.h"
#import "EABrandDetailTableViewController.h"
#import "EABrandListRequest.h"
#import "EAAdressesTableViewController.h"

@interface EABrandListTableViewController ()

@end

@implementation EABrandListTableViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Fiyuu";
    UIBarButtonItem* addressButton=[[UIBarButtonItem alloc] initWithTitle:@"Adreslerim" style:UIBarButtonItemStyleDone target:self action:@selector(navigateAddressList)];
    self.navigationItem.rightBarButtonItem=addressButton;
    [self mainRequest];
}

-(void)mainRequest
{
    [EARequestManager request:[EABrandListRequest new] callback:^(id data, BOOL success) {
        if (success) {
            self.response=[EABrandListResponse arrayOfModelsFromDictionaries:data error:nil];
            [self displayData];
        }
    }];
}

-(void)displayData
{
    [self.tableView reloadData];
}


#pragma mark tableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EABrandListCell* cell = [tableView dequeueReusableCellWithIdentifier:@"EABrandListCell" forIndexPath:indexPath];
    EABrandListResponse* item = [self.response objectAtIndex:indexPath.row];
    [cell setModelWithModel:item];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EABrandListResponse* item = [self.response objectAtIndex:indexPath.row];
    EABrandDetailTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EABrandDetailTableViewController"];
    vc.brandId=item.brandId;
    [self.navigationController pushViewController:vc animated:true];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.response count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)navigateAddressList
{
    EAAdressesTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EAAdressesTableViewController"];
    [self.navigationController pushViewController:vc animated:true];
}


@end
