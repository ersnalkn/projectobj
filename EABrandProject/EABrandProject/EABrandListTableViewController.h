//
//  EABrandListTableViewController.h
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EABrandListResponse.h"

@interface EABrandListTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic)IBOutlet UITableView* tableView;

@property(nonatomic,strong) NSArray<EABrandListResponse *>* response;

@end
