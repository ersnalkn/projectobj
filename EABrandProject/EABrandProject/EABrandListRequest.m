//
//  EABrandListRequest.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandListRequest.h"


@implementation EABrandListRequest

-(NSString*)requestUrl
{
    return @"customer/product/scanBrands-1.0";
}

-(HttpMethod)method
{
    return POST;
}


@end
