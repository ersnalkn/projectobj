//
//  EABrandListResponse.h
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EAResponse.h"

@interface EABrandListResponse : EAResponse

@property(nonatomic)NSString* cover;
@property(nonatomic)NSString* brandId;
@property(nonatomic)NSString* image;
@property(nonatomic)NSString* name;
@property(nonatomic)NSString* kitchenDisplay;

@end
