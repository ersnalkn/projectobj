//
//  EABrandDetailResponse.h
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EAResponse.h"
#import <JSONModel.h>

@interface EABrandProductDto : JSONModel

@property(nonatomic)NSString<Optional>* summary;
@property(nonatomic)NSArray<NSString*><Optional>* images;
@property(nonatomic)NSString<Optional>* name;
@property(nonatomic)NSNumber<Optional>* price;

@end


@interface EABrandDetailResponse : EAResponse

@property(nonatomic)NSString* name;
@property(nonatomic)NSString* image;
@property(strong,nonatomic)NSArray<EABrandProductDto*> * products;
@property(nonatomic)NSNumber* kitchenDisplay;

@end





