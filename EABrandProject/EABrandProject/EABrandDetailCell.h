//
//  EABrandListCell.h
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EABrandDetailResponse.h"

@interface EABrandDetailCell : UITableViewCell

@property(nonatomic)IBOutlet UIImageView* brandImage;
@property(nonatomic)IBOutlet UILabel* brandNameLabel;
@property(nonatomic)IBOutlet UILabel* brandCategoryLabel;


-(void)setModelWithModel:(EABrandProductDto*)model;

@end
