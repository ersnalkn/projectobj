//
//  EABrandListCell.m
//  EABrandProject
//
//  Created by ersun alkan on 18.09.2018.
//  Copyright © 2018 ersun alkan. All rights reserved.
//

#import "EABrandListCell.h"
#import <UIImageView+AFNetworking.h>
#import <UIImage+AFNetworking.h>

@implementation EABrandListCell


-(void)setModelWithModel:(EABrandListResponse*)model
{
    self.brandImage.image=nil;
    self.brandNameLabel.text=model.name;
    self.brandCategoryLabel.text=model.kitchenDisplay;
    [self.brandImage setImageWithURL:[NSURL URLWithString:model.image]];
    
}

@end
